ymaps.ready(function() {
  var markers = [
    {
      icon: 'img/icons/marker.svg',
      position: [55.812807, 37.835081],
    },
  ];
  init('map_1', [55.812807, 37.835081], markers);
});

ymaps.ready(function() {
  var markers = [
    {
      icon: 'img/icons/marker.svg',
      position: [55.905828, 38.002487],
    },
  ];
  init('map_2', [55.905828, 38.002487], markers);
});

function init(map_id, latLang, markers) {
  var myMap;
  myMap = new ymaps.Map(map_id, {
    center: latLang,
    zoom: 15,
  });

  myMap.behaviors.disable('scrollZoom');

  myMap.controls.add('zoomControl', {
    position: { top: 15, left: 15 },
  });

  markers.forEach(function(mark) {
    var myPlacemark = new ymaps.Placemark(mark.position);
    myMap.geoObjects.add(myPlacemark);
  });
}


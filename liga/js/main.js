$(function() {
	scrollHeader();
	dropdownNavHandler();
	closeWidgetMain();
	searchFunctionality();
	initSliders();
	initAboutSlider();
	initDatePicker();
	initCustomSelect();
	videoPlay();
	tabs();
	equipmentType();
	tooltip();
	validationForm();
	popover();
	initSliderInImg();
	initGallerySlider();
	inputPlaceholder();
	searchPlaceholderChange();
	searchMob();
	mobMenu();
	mobCatalogNav();
	equipmentFilterMob();
	modelChangeSlider();
	pageDataMobContent();
	mobModelMenu();
	scrollToBlock();
	stickyInit();
	similarSliderInit();
	initEngineeringSlider();
	initArticleSlider();
})


function scrollHeader() {
	$(window).scroll(function() {
		if ($(window).scrollTop() > 250) {
			$('.js-header').addClass('fixed');
			$('.js-catalog').addClass('fixed');
			$(".js-search input[type=search]").attr("placeholder", "Поиск...");

		
		} else {
			$('.js-header.fixed').removeClass('fixed');
			$('.js-catalog').removeClass('fixed');
			$('.js-search .search-result').removeClass('show');
			$('.js-search .input-search').removeClass('active');
			$('.js-search .js-search-clear').removeClass('active');
			searchPlaceholderChange();
		}
		
	});
}

function dropdownNavHandler() {

	$('.js-catalog').hover(function() {
		that = $(this);
		menuHoverHandler();
	}); 

	var timer;
	var delay = 300;

	$('.js-dropdown-nav-catalog .js-close-catalog').click(function(e) {
		e.preventDefault();
		if ($(this).closest(".js-dropdown-nav-catalog").hasClass("show")) {
			$('.js-dropdown-nav-catalog').removeClass('show')
			$(this).closest(".js-dropdown-nav-catalog").addClass("disabled");
			$('.js-catalog').removeClass('show');
			$('.js-header-oveflow').removeClass('overflow-hidden');
		}
	});

	$('.js-dropdown-nav-catalog .js-btn').hover(function(e) {

			that = $(this);
			timer = setTimeout(function(e) {
				$(".js-dropdown-nav-catalog").removeClass("disabled");
				menuHoverHandler();
			}, delay);
	}, function() {

			clearTimeout(timer);
	});

	$('.js-header-bottom-nav').mouseleave(function(e) {
		$(".js-dropdown-nav-catalog").removeClass("disabled");
	});

}

function menuHoverHandler(e) {

	var catalog = that.attr('data-catalog-name');

	if (that.parent().hasClass('show')) {
		that.parent().removeClass('show');
		$('.js-catalog').removeClass('show');
		$('.js-header-oveflow').removeClass('overflow-hidden');
	} else {
		$('.js-dropdown-nav-catalog').removeClass('show');
		$('.js-catalog').addClass('show');
		$('.js-header-oveflow').addClass('overflow-hidden');
		that.parent().addClass('show');
	}

	/*
		вешаем активный атрибут на каталог (js-catalog)
		и когда наводим на каталог есть нужный атрибут для отображения элементов
	*/
	$('.js-catalog').attr('data-catalog-name', catalog);

	if(that.hasClass('js-catalog')) {
		$('.js-dropdown-nav-catalog .js-btn').each(function(ind, el) {
			if($(el).attr('data-catalog-name') === catalog && that.hasClass('show')) {
				$(el).parent().addClass('show');
			} else {
				$(el).parent().removeClass('show');
			}
		})
	}

	$('.js-catalog .js-catalog-item').hide();

	$('.js-catalog .js-catalog-item').each(function() {
		var data = $(this).attr('data-catalog');
		data = data.split(' ');

		for (var i = 0; i < data.length; i++) {
			if (data[i] == catalog) {
				$(this).show();
			}
		}
	});
}


function closeWidgetMain() {
	$(document).mouseup(function(e) {
		closeWidgetHandler(e, $('.js-catalog'), $('.js-header-bottom-nav'));
	});
}

function closeWidgetHandler(e, target, target_2) {
	var div = target;
	var div_2 = target_2;
	if (!div.is(e.target) && div.has(e.target).length === 0 && !div_2.is(e.target) && div_2.has(e.target).length === 0) {
		$('.js-catalog').removeClass('show');
		$('.js-header-oveflow').removeClass('overflow-hidden');
		$('.js-dropdown-nav-catalog').removeClass('show');
	}
}

function searchFunctionality() {
	$('.js-search .input-search input').keypress(function() {
		$(this).closest('.js-search').find('.search-result').addClass('show');
		$(this).closest('.js-search').find('.input-search').addClass('active');
		$(this).closest('.input-search').find('.js-search-clear').addClass('active');

		if ($(window).width() < 991.98) {
			$('.js-search-result-mob').addClass('show');
			$(".js-body-overlay").addClass('show').addClass("for-search");
		}
		
	});

	$(document).mouseup(function (e){ 
		var div = $(".js-search .input-search"); 
		var div_2 = $(".js-search-result-mob");
		if (!div.is(e.target) && div.has(e.target).length === 0 && !div_2.is(e.target) && div_2.has(e.target).length === 0) { 
			$('.js-search .search-result').removeClass('show');
			$('.js-search .input-search').removeClass('active');
			$('.js-search .js-search-clear').removeClass('active');
			$(".js-search-result-mob").removeClass('show');
			$(".js-body-overlay.show.for-search").removeClass('show').removeClass("for-search");
		}

	});

	$('.js-search .js-search-clear').on('click', function(e) {
		e.preventDefault();
		$('.js-search .input-search').find('.form-control').val("");
		$('.search-result').removeClass('show');
		$('.js-search').find('.input-search').removeClass('active');
		$('.input-search').find('.js-search-clear').removeClass('active');
		if ($(window).width() < 992 && $(window).width() > 575.98) {
			$(".js-body-overlay.show.for-search").removeClass('show').removeClass("for-search");
		}

		if ($(window).width() < 768) {
			$('.js-search').removeClass('active');
			$('.js-search .search-result').removeClass('show');
			$('.js-search .input-search').removeClass('active');
			$('.js-search .js-search-clear').removeClass('active');
			$(".js-search-result-mob").removeClass('show');
			$(".js-body-overlay.show.for-search").removeClass('show').removeClass("for-search");
		}
		
	});
}

function initSliders() {
	if($('.js-row-slider').length) {
		$('.js-row-slider-4-slides').slick({
			arrows: false,
			dots: false,
			slidesToShow: 4,
			slidesToScroll: 1,
			autoplay: false,
			infinite: false,

			responsive: [
				{
					breakpoint: 1199.98,
					settings: {
						slidesToShow: 3,
					}
				},
				{
					breakpoint:767.98,
					settings: {
						slidesToShow: 3,
					}
				},
				{
					breakpoint:575.98,
					settings: {
						slidesToShow: 2,
					}
				},
				{
					breakpoint:440,
					settings: {
						slidesToShow: 1,
					}
				}

			]
		});

		var fiveSlides = $('.js-row-slider-5-slides');
		fiveSlides.slick({
			arrows: false,
			dots: false,
			slidesToShow: 5,
			slidesToScroll: 1,
			autoplay: false,
			infinite: false,
			responsive: [
				{
					breakpoint: 1199.98,
					settings: {
						slidesToShow: 4,
					}
				},

				{
					breakpoint: 575.98,
					settings: {
						slidesToShow: 3,
					}
				},

				{
					breakpoint: 440,
					settings: {
						slidesToShow: 2,
					}
				}
			]
		});

		fiveSlides.on('breakpoint', function(event, slick, breakpoint) {
			if ((fiveSlides.find('.slick-slide').length <= fiveSlides.get(0).slick.options.slidesToShow)) {
				fiveSlides.closest('.js-slider-nav-event').find('.row-slider-nav').hide();
			} else {
				fiveSlides.closest('.js-slider-nav-event').find('.row-slider-nav').show();
			}
		});

		if ((fiveSlides.find('.slick-slide').length <= fiveSlides.get(0).slick.options.slidesToShow)) {
			fiveSlides.closest('.js-slider-nav-event').find('.row-slider-nav').hide();
		} else {
			fiveSlides.closest('.js-slider-nav-event').find('.row-slider-nav').show();
		}
	}


	$('.js-slider-nav-event .row-slider-nav .js-carousel-control-prev').addClass('slick-disabled');

	$('.js-slider-nav-event .js-carousel-control-prev, .js-slider-nav-event .js-carousel-control-next').on("click", stateNavSlider);

}

function initGallerySlider() {
	if($('.js-product-gallery-main-slider').length) {
		$('.js-product-gallery-main-slider').slick({
			arrows: true,
			dots: false,
			slidesToShow: 1,
			slidesToScroll: 1,
			autoplay: false,
			infinite: false,
			asNavFor: $(".js-product-gallery-nav-slider"),
			responsive: [
				{
					breakpoint: 991.98,
					settings: {
						asNavFor: "",
						dots: true,
						arrows: false,
					}
				},
			],
		});
	}

	if($('.js-product-gallery-nav-slider').length) {
		$('.js-product-gallery-nav-slider').slick({
			arrows: false,
			dots: false,
			slidesToShow: 6,
			slidesToScroll: 1,
			autoplay: false,
			infinite: false,
			asNavFor: $(".js-product-gallery-main-slider"),
			focusOnSelect: true,
			swipe: false
			
		});

	}

	if($('.js-about-info-slider').length) {
		$('.js-about-info-slider').slick({
			arrows: true,
			dots: false,
			slidesToShow: 1,
			slidesToScroll: 1,
			autoplay: false,
			infinite: false,
			responsive: [
				{
					breakpoint: 991.98,
					settings: {
						asNavFor: "",
						dots: true,
						arrows: false,
					}
				},
			],
		});
	}


}

function initSliderInImg() {

	if($('.js-slider-behind-img').length) {
		$('.js-slider-behind-img').slick({
			arrows: false,
			dots: false,
			slidesToShow: 1,
			slidesToScroll: 1,
			autoplay: false,
			infinite: false,
			swipe: false,
			fade: true,
		});
	}

	if($('.js-slider-front-desc').length) {
		$('.js-slider-front-desc').slick({
			arrows: true,
			dots: false,
			slidesToShow: 1,
			slidesToScroll: 1,
			autoplay: false,
			infinite: false,
			asNavFor: $(".js-slider-behind-img"),
			focusOnSelect: true,
			swipe: false,
			fade: true,
			prevArrow: $(".js-slider-in-img .js-slick-prev"),
			nextArrow: $(".js-slider-in-img .js-slick-next"),
		});
	}

	var thisSlider = $('.js-slider-front-desc');
	
	if (thisSlider.length) { 
		var currensSlide = thisSlider.slick('slickCurrentSlide')+1; 
		var totalSlides = thisSlider.slick("getSlick").slideCount;
		thisSlider.closest(".js-slider-in-img").find(".js-slide-of").html(currensSlide + '/' + totalSlides);
	}

	$('.js-slider-front-desc').on('afterChange', function(event, slick, currentSlide, nextSlide){
		var currensSlide = $(this).slick('slickCurrentSlide')+1;
		var totalSlides = $(this).slick("getSlick").slideCount;
		$(this).closest(".js-slider-in-img").find(".js-slide-of").html(currensSlide + '/' + totalSlides);
	});
}


function initAboutSlider() {
	var achSlider = $('.js-achievements-slider');
	var achSliderNav = $('.js-achievements-slider-nav');

	if(achSlider.length && achSliderNav.length) {
		achSlider
			.slick({
				centerMode: true,
				centerPadding: '0%',
				arrows: true,
				dots: false,
				slidesToShow: 3,
				slidesToScroll: 1,
				autoplay: false,
				prevArrow: $('.about__achievements .js-carousel-control-prev'),
				nextArrow: $('.about__achievements .js-carousel-control-next'),
				asNavFor: achSliderNav,

				responsive: [
					{
						breakpoint: 767.98,
						settings: {
							slidesToShow: 2,
						}
					},

					{
						breakpoint: 440,
						settings: {
							slidesToShow: 1,
						}
					}
				]
			});

		achSliderNav
			.slick({
				centerMode: true,
				centerPadding: '0%',
				arrows: false,
				dots: false,
				slidesToShow: 9,
				slidesToScroll: 1,
				autoplay: false,
				focusOnSelect: true,
				asNavFor: achSlider,
				responsive: [
					{
						breakpoint: 1199.98,
						settings: {
							slidesToShow: 8,
						}
					},

					{
						breakpoint: 991.98,
						settings: {
							slidesToShow: 5,
						}
					}
				]
			})
			.find(".slick-slide").on("click", function(){
				achSliderNav.slick("slickNext");
			});
	}
}

function initEngineeringSlider() {
	var samSlider = $('.js-sample-projects-slider');
	var samSliderNav = $('.js-sample-projects-slider-nav');

	if(samSlider.length && samSliderNav.length) {
		samSlider
			.slick({
				arrows: true,
				dots: false,
				slidesToShow: 1,
				slidesToScroll: 1,
				autoplay: false,
				fade: true,
				prevArrow: $('.about__sample-projects .js-carousel-control-prev'),
				nextArrow: $('.about__sample-projects .js-carousel-control-next'),
				asNavFor: samSliderNav,
				infinite: false,
				responsive: [
					{
						breakpoint: 767.98,
						settings: {
							dots: true,
						}
					},
				]
			});

		samSliderNav
			.slick({
				// centerMode: true,
				// centerPadding: '0%',
				arrows: false,
				dots: false,
				slidesToShow: 9,
				slidesToScroll: 1,
				autoplay: false,
				focusOnSelect: true,
				asNavFor: samSlider,
				infinite: false,
				responsive: [
					{
						breakpoint: 767.98,
						settings: {
							dots: true,
						}
					},
				]
			})
			.find(".slick-slide").on("click", function(){
				samSliderNav.slick("slickNext");
			});
	}
}

function initArticleSlider() {
	var articleSliderMain = $('.js-article-slider-main');
	var articleSliderNav = $('.js-article-slider-nav');

	if(articleSliderMain.length && articleSliderNav.length) {
		articleSliderMain
			.slick({
				arrows: true,
				dots: false,
				slidesToShow: 1,
				slidesToScroll: 1,
				autoplay: false,
				fade: true,
				prevArrow: $('.article-sliders-wrap .js-carousel-control-prev'),
				nextArrow: $('.article-sliders-wrap .js-carousel-control-next'),
				asNavFor: articleSliderNav,
				infinite: false,
				responsive: [
					{
						breakpoint: 767.98,
						settings: {
							dots: true,
						}
					},
				]
			});

		articleSliderNav
			.slick({
				// centerMode: true,
				// centerPadding: '0%',
				arrows: false,
				dots: false,
				slidesToShow: 5,
				slidesToScroll: 1,
				autoplay: false,
				focusOnSelect: true,
				asNavFor: articleSliderMain,
				infinite: false,
				// responsive: [
				// 	{
				// 		breakpoint: 1199.98,
				// 		settings: {
				// 			slidesToShow: 3,
				// 		}
				// 	},

				// 	{
				// 		breakpoint: 991.98,
				// 		settings: {
				// 			slidesToShow: 2,
				// 		}
				// 	}
				// ]
			})
			// .find(".slick-slide").on("click", function(){
			// 	articleSliderNav.slick("slickNext");
			// });
	}
}



function initDatePicker() {
	if($('input[name="dates"]').length) {
		$('input[name="dates"]').daterangepicker({
			"autoApply": true,
			"opens": 'left',
			"locale": {
					"format": "DD.MM.YYYY",
					"separator": " по ",
					"applyLabel": "Принять",
					"cancelLabel": "Новости за все время",
					"fromLabel": "От",
					"toLabel": "До",
					"customRangeLabel": "Custom",
					"weekLabel": "Н",
					"daysOfWeek": [
							"Вс",
							"Пн",
							"Вт",
							"Ср",
							"Чт",
							"Пт",
							"Сб"
					],
					"monthNames": [
							"Январь",
							"Февраль",
							"Март",
							"Апрель",
							"Май",
							"Июнь",
							"Июль",
							"Авуст",
							"Сентябрь",
							"Октябрь",
							"Ноябрь",
							"Декабрь"
					],
					"firstDay": 1
			},
		});

		$(".daterangepicker .drp-buttons .btn.cancelBtn").on("click", function(e) {
			$(".js-date-change__input-wrap").addClass("hide-input");
			$(".js-date-change__time").html("все время");
			$('.js-date-change__input').val('');
		});

		$('.js-date-change__input').on('apply.daterangepicker', function(ev, picker) {
			$(".js-date-change__input-wrap").removeClass("hide-input");
			$(".js-date-change__time").html("c");
		});
	}
}


function initCustomSelect() {
	if($('.js-custom-select').length) {
		$('.js-custom-select').customSelect({
			block: "custom-select-one",
			hideCallback: function () {
				var $value = $(this).find('[class$="value"]');
				// console.log(`Dropdown closed!`);
				// console.log($value.text());
			},
			showCallback: function () {
				// console.log('Dropdown opened!');
			}
		});
	}
}

function videoPlay() {
	$(document).on("click", ".js-play", function(e) {
		e.preventDefault();
		var videoSrc = $(this).attr("data-videoSrc");
		$(this).closest(".video-wrap").find('.video').attr('src', ''+videoSrc+'?autoplay=1&rel=0').addClass("visible");
	});

	$(document).on("click", ".js-play-in-modal", function(e) {
		e.preventDefault();
		var videoSrc = $(this).attr("data-videoSrc");
		var videoTitle = $(this).attr("data-title");
		$(".js-modal-video .js-video").attr('src',  videoSrc + '?autoplay=1&rel=0');
		$(".js-modal-video .js-modal-title").text(videoTitle);

	});


	$('.js-modal-video').on('hidden.bs.modal', function (e) {
		$(".js-modal-video .js-video").removeAttr('src');
		$(".js-modal-video .js-modal-title").text("");
	});



}

function tabs() {



	$(".js-filters a").on("click", function(e) {

		e.preventDefault();

		$(this).parent().find("a").removeClass("active");
		$(this).addClass("active");

		var dataTab = $(this).attr("data-tab");
		$(this).closest(".js-tabs-wrap").find(".js-tab-item").removeClass("active");
		$(this).closest(".js-tabs-wrap").find(".js-tab-item").each(function() {
			var dataTabItem = $(this).attr("data-tab-item");
			if (dataTab == dataTabItem) {
				$(this).addClass("active");
			}
		});

	});

	
	$(".js-filters a").on("click", stateNavSlider);


	$(".js-filters-dropdown a").click(function(){
		var dataTabName = $(this).attr("data-tab-name");
		$(this).closest(".js-filters-dropdown").find(".dropdown-toggle").text(dataTabName);
	});
}


function stateNavSlider() {
	var thisSlider = $(this).closest('.js-slider-nav-event').find('.js-tab-item.active .js-row-slider');
			
	if(thisSlider.length) {
		if ($(this).hasClass("js-carousel-control-prev")) {
			thisSlider.slick('slickPrev');
		}

		if ($(this).hasClass("js-carousel-control-next")) {
			thisSlider.slick('slickNext');
		}
		var thisSliderPrev = thisSlider.closest('.js-slider-nav-event').find(".js-row-slider-nav .js-carousel-control-prev");
		var thisSliderNext = thisSlider.closest('.js-slider-nav-event').find(".js-row-slider-nav .js-carousel-control-next");

		var currentSlide = thisSlider.slick('slickCurrentSlide');
		
		var slidesCount = thisSlider.slick("getSlick").slideCount;
		var  slidesCountVisible = thisSlider.slick("getSlick").options.slidesToShow;


		if (currentSlide == 0) {
			thisSliderPrev.addClass("slick-disabled");
		} else {
			thisSliderPrev.removeClass("slick-disabled");
		}

		if (currentSlide == slidesCount - slidesCountVisible) {
			
			thisSliderNext.addClass("slick-disabled");
		} else {
			thisSliderNext.removeClass("slick-disabled");
		}
	}
}

function equipmentType() {

	equipmentTypeActive();

	function equipmentTypeActive() {
		
		activeEquipmentBtn = 0
		$(".js-equipment-types-wrap .js-equipment-type").each(function() {
			if ($(this).hasClass("active")) {
				activeEquipmentBtn++;
			} 
		});

		if (activeEquipmentBtn > 0) {
			$(".js-equipment-types-wrap .js-remove-all").addClass("visible");
		} else {
			$(".js-equipment-types-wrap .js-remove-all").removeClass("visible");
		}
	}

	
	$(".js-equipment-type__btn-left").on("click", function() {

		var equipmentBtn = $(this).closest(".js-equipment-type");
		equipmentBtn.removeClass("has-select").removeClass("active-right-btn");

		if (equipmentBtn.hasClass("active")) {
			equipmentBtn.removeClass("active");
			equipmentBtn.find("input").prop('checked', false);
		}  else {
			equipmentBtn.addClass("active");
			equipmentBtn.find("input").prop('checked', true);
		}

		equipmentTypeActive();

	});


	$(".js-equipment-type__dropdown").mouseleave("click", function() {
		$(".js-equipment-type").removeClass("active-right-btn");
	});

	$(".equipment-type__dropdown input").change(function() {

		var equipmentBtn = $(this).closest(".js-equipment-type");
		var inputCheck = 0;
		var inputQuantity = 0;
		equipmentBtn.find(".equipment-type__dropdown input").each(function() {
			inputQuantity++
			if ($(this).prop('checked')) {
				inputCheck++;
			} 
		});

		if (inputCheck > 0) {
			$(this).closest(".js-equipment-type").addClass("active");
			equipmentBtn.find(".js-checkbox-radio-for-btn input").prop('checked', true);
			equipmentBtn.addClass("has-select");
		} else {
			equipmentBtn.removeClass("has-select");
		}

		if (inputCheck == inputQuantity) {
			equipmentBtn.removeClass("has-select");
		}

		if (inputCheck == 0) {
			equipmentBtn.removeClass("has-select").removeClass("active");
			equipmentBtn.find(".js-checkbox-radio-for-btn input").prop('checked', false);
		}

		equipmentTypeActive();

	});


	$(".js-equipment-type__btn-right").on("click", function(e) {
		var equipmentBtn = $(this).closest(".js-equipment-type");
		equipmentBtn.toggleClass("active-right-btn");

	});

	$(document).click(function(e){
		var div = $(".js-equipment-type");
		if (!div.is(e.target)
				&& div.has(e.target).length === 0) {
			$(".js-equipment-type").removeClass("active-right-btn");
		}
	});

	$(".js-remove-all").on("click", function() {
		$(".js-equipment-types-wrap .js-equipment-type").removeClass("active").removeClass("has-select").find("input").prop('checked', false);
		equipmentTypeActive();
	});

}

function tooltip() {
	$('.tooltip-js').tooltip();
}

function validationForm() {

	$('.js-form button[type=submit]').click(function(e) {

		var input = $(this).closest(".js-form").find("input");
		input.each(function() {
			var val = $(this).val();
			if (val == "") {
				$(this).addClass("is-invalid");
				$(this).closest(".js-form").find("button[type=submit]").attr("disabled","");
			} else {
				$(this).removeClass("is-invalid");
			}
		});

		$(".js-form").find("input").focus(function(){
			$(this).removeClass("is-invalid");
			$(this).closest(".js-form").find("button[type=submit]").removeAttr("disabled");
		});
		
	});
}


function popover() {
	$('.js-popover').popover({
		container: 'body',
		trigger: 'hover',
		html: true,
		offset: 120,
	})
}

function inputPlaceholder() {
	$(".carrying-form input").focus(function() {
		$(this).closest(".form-group").addClass("used");
	});
	$(".carrying-form .form-group").click(function() {
		$(this).find("input").focus();
	});

	$(".carrying-form input").each(function()  {
			var val = '';
			val = $(this).val();
		
			if (val == '') {
				$(this).closest(".form-group").removeClass("used");
				
			} else {
				$(this).closest(".form-group").addClass("used");
			}
	 });

	$(".carrying-form input").focusout(function() {
		var val = '';
		val = $(this).val();
	
		if (val == '') {
			$(this).closest(".form-group").removeClass("used");
			
		} else {
			$(this).closest(".form-group").addClass("used");
		}
	});
}

function searchPlaceholderChange() {
	if ($(window).width() > 1199.98) {
		$(".js-search input[type=search]").attr("placeholder", "Поиск оборудования, станков, инструмента...");
	}
	if ($(window).width() < 1199.98) {
		$(".js-search input[type=search]").attr("placeholder", "Поиск...");
	}

	if ($(window).width() < 991.98) {
		$(".js-search input[type=search]").attr("placeholder", "Поиск оборудования, станков...");
	}

	if ($(window).width() < 767.98) {
		$(".js-search input[type=search]").attr("placeholder", "Поиск...");
	}


	if ($(window).width() < 575.98) {
		$(".js-search input[type=search]").attr("placeholder", "Поиск оборудования, станков...");
	}
}


function searchMob() {
	$(".js-search-btn").click(function(e) {
		e.preventDefault();
		$(".js-search").addClass('active').find("input").focus();
		$('.js-search-clear').addClass('active');
		$(".js-body-overlay").addClass('show').addClass('for-search');
	});

	$(document).click(function(e){
		var div = $(".js-search");
		var div2 = $(".js-search-btn");
		if (!div.is(e.target) && div.has(e.target).length === 0 && !div2.is(e.target) && div2.has(e.target).length === 0) {
			$(".js-search").removeClass("active");
			$(".js-body-overlay.show.for-search").removeClass('show').removeClass('for-search');
			$('.js-search-clear').removeClass('active');
		}
	});
}

function mobMenu() {
	$(".js-mob-menu-btn").click(function(e) {
		e.preventDefault();
		$(".js-mob-menu").addClass("show");
		$(".js-body-overlay").addClass("show").addClass("for-mob-menu");
		$('.js-body').addClass('body-overflow-hidden');
	});

	$(".js-mob-menu-close").click(function(e) {
		e.preventDefault();
		$(".js-mob-menu").removeClass("show");
		$(".js-body-overlay.show.for-mob-menu").removeClass("show").removeClass("for-mob-menu");
		$('.js-mob-menu .sub-menu-show').removeClass("sub-menu-show");
		$('.js-body').removeClass('body-overflow-hidden');
	});
}

function mobCatalogNav() {
	$(".js-mob-nav-catalog .js-btn").click(function(e) {
		e.preventDefault();
		var catalog = $(this).attr('data-catalog-name');
		$('.js-mob-nav-catalog-type').attr('data-catalog-name', catalog);

		$('.js-mob-nav-catalog-type > li').hide();

		$('.js-mob-nav-catalog-type > li').each(function() {
			var data = $(this).attr('data-catalog');
			data = data.split(' ');

			for (var i = 0; i < data.length; i++) {
				if (data[i] == catalog) {
					$(this).show();
				}
			}
		});

		$('.js-sub-menu-catalog').addClass("sub-menu-show");

	});

	$(".js-call-sub-menu").click(function(e) {
		e.preventDefault();
		$(".js-mob-menu__main").scrollTop(0);
		$(this).parent().find(".js-sub-menu").addClass("sub-menu-show");
	});

	$('.js-sub-menu-back').click(function(e) {
		e.preventDefault();
		$(this).closest('.sub-menu-show').removeClass("sub-menu-show");
	});

}

function equipmentFilterMob() {

	function activeMainBtn() {
		var inputCheck = 0;

		$('.js-mob-equipment-filter  .js-category-checkbox').each(function() {

			if ($(this).prop('checked')) {
				inputCheck++;
			} 
		});

		if (inputCheck > 0) {
			$('.js-mob-equipment-type-btn').addClass("active");
			$('.js-mob-equipment-type-btn-label').html("").text("Выбрано категорий: " + inputCheck);
		} else {
			$('.js-mob-equipment-type-btn').removeClass("active");
			$('.js-mob-equipment-type-btn-label').html("").text("Выбрать категории");
		}
	}

	$('.js-mob-equipment-type-btn').on("click", function() {
		$('.js-mob-equipment-filter').addClass("show");
		$('.js-body').addClass('body-overflow-hidden');

	});

	$('.js-mob-equipment-filter .js-close').on("click", function(e) {
		e.preventDefault();
		$('.js-mob-equipment-filter').removeClass("show");
		$('.js-subtypes-list').removeClass("show");
		$('.js-body').removeClass('body-overflow-hidden');
	});

	$('.js-mob-equipment-filter .js-appy').on("click", function(e) {
		e.preventDefault();
		$('.js-mob-equipment-filter').removeClass("show");
		$('.js-subtypes-list').removeClass("show");
		activeMainBtn();
		var pageTitleDesc = "";
		var equipmentTypes = "";
		var equipmentSubTypes = "";

		$('.js-subtypes').each(function() {

			equipmentTypes = "";
			equipmentSubTypes = "";
			
			$(this).find('input[type=checkbox]').each(function() {
			
				if ($(this).prop('checked')) {

					if ($(this).attr("data-equipment-type")) {
						equipmentTypes = $(this).attr("data-equipment-type");   
					}

					if ($(this).attr("data-equipment-subtype")) {
						equipmentSubTypes = equipmentSubTypes + $(this).attr("data-equipment-subtype") + ", ";
					} 
				}

			});
			
			if (equipmentSubTypes) {
				equipmentSubTypes = equipmentSubTypes.substring(0, equipmentSubTypes.length - 2);
				equipmentSubTypes = " (" + equipmentSubTypes + ")";
			}

			pageTitleDesc =  pageTitleDesc + equipmentTypes + equipmentSubTypes;

			if (pageTitleDesc && equipmentTypes) {
				pageTitleDesc =  pageTitleDesc + ", ";
			}
		});


		pageTitleDesc = pageTitleDesc.substring(0, pageTitleDesc.length - 2);
		

		if (pageTitleDesc) {
			$('.js-page-title-separate').text(",");
			$('.js-page-title-desc').text(pageTitleDesc);
		} else {
			$('.js-page-title-separate').text("");
			$('.js-page-title-desc').text("");
		}

		$('.js-body').removeClass('body-overflow-hidden');

	});

	$('.js-mob-equipment-filter .js-discard-btn').on("click", function(e) {
		e.preventDefault();
		$(this).closest(".js-mob-equipment-filter").find('input[type=checkbox]').prop('checked', false);
		$(".js-selected-types").html("");


	});

	$('.js-mob-equipment-filter .js-equipment-subtypes').on("click", function(e) {
		e.preventDefault();
		$(".equipment-filter__main").scrollTop(0);
		$(this).closest(".js-subtypes").find('.js-subtypes-list').addClass("show");
	});

	$('.js-mob-equipment-filter .js-subtypes-back').on("click", function(e) {
		e.preventDefault();
		$(this).closest(".js-subtypes").find('.js-subtypes-list').removeClass("show");
	});

	$('.js-category-checkbox').click(function(e) {
		var subtypesCheckbox = $(this).closest(".js-subtypes").find('.js-subtypes-list input[type=checkbox]');
		var selectedTypes = '';

		if ($(this).prop('checked')) {
			subtypesCheckbox.prop('checked', true);
			subtypesCheckbox.each(function() {
				selectedTypes = selectedTypes  + $(this).attr("data-equipment-subtype") + ", ";
			});
		
		} else {
			subtypesCheckbox.prop('checked', false);
			selectedTypes = '';
		}

		if (selectedTypes.length) {
			selectedTypes = selectedTypes.substring(0, selectedTypes.length - 2);
		}
		$(this).closest(".js-subtypes").find(".js-selected-types").text(selectedTypes);
		
	});

	$('.js-subtypes-list input[type=checkbox]').change(function(e) {
		var subtypesCheckbox = $(this).closest(".js-subtypes").find('.js-subtypes-list input[type=checkbox]');
		var inputCheck = 0;
		var selectedTypes = '';
		subtypesCheckbox.each(function() {

			if ($(this).prop('checked')) {
				inputCheck++;
				selectedTypes = selectedTypes  + $(this).attr("data-equipment-subtype") + ", ";
			} 

			if (inputCheck > 0) {
				$(this).closest(".js-subtypes").find(".js-category-checkbox").prop('checked', true);
			} else {
				$(this).closest(".js-subtypes").find(".js-category-checkbox").prop('checked', false);
			}
		});

		if (selectedTypes.length) {
			selectedTypes = selectedTypes.substring(0, selectedTypes.length - 2);
		}
		$(this).closest(".js-subtypes").find(".js-selected-types").text(selectedTypes);
	});
}


function modelChangeSlider() {

	if($('.js-model-slider').length) {
		$('.js-model-slider').slick({
			arrows: false,
			dots: false,
			slidesToShow: 2,
			slidesToScroll: 1,
			autoplay: false,
			infinite: false,
			swipe: true,
			centerMode: true,
			// infinite: true,
			initialSlide: 1,
			responsive: [
        {
          breakpoint: 767.98,
          settings: {
            slidesToShow: 1,
            initialSlide: 0,
          }
        },
       ]

		});
	}


}

function pageDataMobContent() {
	$('.js-mob-content-call').on("click", function(e) {
		e.preventDefault();
		$('.js-page-data-mob-content').addClass('show');
		$('.js-body').addClass('body-overflow-hidden');
	});


	$('.js-mob-content-close').on("click", function(e) {
		e.preventDefault();
		$('.js-page-data-mob-content').removeClass('show');
		$('.js-more-link').removeClass('active').attr("aria-selected", false);
		$('.js-mob-content-call .nav-link').removeClass('active').attr("aria-selected", false);
		$('.js-body.body-overflow-hidden').removeClass('body-overflow-hidden');
		
	});
}

function mobModelMenu() {

	$(".model-change").delegate(".js-model-menu-call", "click", function(e) {
		e.preventDefault();
		$('.js-mob-model-menu').addClass('show');
		$(".js-body-overlay").addClass('show').addClass('for-mob-model-menu');
	});

	$("body").delegate(".js-mob-model-menu .js-close", "click", function(e) {
	 e.preventDefault();
		$('.js-mob-model-menu').removeClass('show');
		$(".js-body-overlay.for-mob-model-menu").removeClass('show').removeClass('for-mob-model-menu');
	});


}

function scrollToBlock() {
	$('.js-btn-scroll').click( function(){
	var scroll_el = $(this).attr('href');
			if ($(scroll_el).length != 0) { 
			$('html, body').animate({ scrollTop: $(scroll_el).offset().top - $(".js-header-fixed").height() }, 500); 
			}
			return false; 
	});
}

function stickyInit() {
	if($('.js-sticky').length) {
		var sticky = new Sticky('.js-sticky', {
			marginTop: 80,
		});
	}
	
}

function similarSliderInit() {
	if($('.js-mob-slider').length && $(window).width() < 576) {
		$('.js-mob-slider').slick({
			arrows: false,
			dots: false,
			slidesToShow: 2,
			slidesToScroll: 1,
			autoplay: false,
			infinite: false,
			responsive: [

				{
					breakpoint: 440,
					settings: {
						slidesToShow: 1,
					}
				}
			]
		});
	}
}
